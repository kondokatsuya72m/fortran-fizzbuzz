program main
    implicit none

    integer :: N_STEP 
    integer :: i
    character(len=10) :: msg

    N_STEP = 100

    do i=1,N_STEP
        msg = ""

        if (mod(i,3) == 0) then
            msg = trim(msg) // "fizz"
        end if  

        if (mod(i,5) == 0) then
            msg = trim(msg) // "buzz"
        end if  

        write(6,*) i, msg
    end do

end program main
